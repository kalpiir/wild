# -*- coding: utf-8 -*-
"""
Created on Wed Dec  4 19:58:35 2019

@author: Kalle
"""

def getTopSix():
	
	import requests
	import nhlUtil
	
	prefix = 'https://statsapi.web.nhl.com/api/v1/standings/byLeague'
	request = requests.get(prefix)
	r = request.json()
	
	topSix = []
	teamRec = r['records'][0]['teamRecords']
	j = 1
	while j < 31:
		if j < 7:
			team = "{}. {} | PTS: {} | GP: {} | {}-{}-{} | {}".format(j, teamRec[j]['team']['name'], teamRec[j]['points'],teamRec[j]['gamesPlayed'],\
			  teamRec[j]['leagueRecord']['wins'],teamRec[j]['leagueRecord']['losses'],teamRec[j]['leagueRecord']['ot'],teamRec[j]['streak']['streakCode'])
			topSix.append(team)
			
		if teamRec[j]['team']['id'] == nhlUtil.getTeamId('MIN') and j > 6:
			topSix.append('...')
			team = "{}. {} | PTS: {} | GP: {} | {}-{}-{} | {}".format(j, teamRec[j]['team']['name'], teamRec[j]['points'],teamRec[j]['gamesPlayed'],\
			  teamRec[j]['leagueRecord']['wins'],teamRec[j]['leagueRecord']['losses'],teamRec[j]['leagueRecord']['ot'],teamRec[j]['streak']['streakCode'])
			topSix.append(team)
		j += 1
		
	return topSix
	
test = getTopSix()
print(test)