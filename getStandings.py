# -*- coding: utf-8 -*-
"""
Created on Mon Nov  4 19:20:44 2019

@author: Kalle
"""
def getStandings(division):
	import requests
	#from datetime import datetime
	#from pytz import timezone
	#import pytz
	
	availableDivisions = ['Metropolitan', 'Atlantic', 'Central', 'Pacific']
	standings = []
	if division not in availableDivisions:
		standings = ['Non-existent division, try again. Codes: Metropolitan, Atlantic, Central, Pacific']
		return standings
	
	
	prefix = 'https://statsapi.web.nhl.com/api/v1'
	endPoint = '/standings/byDivision'
	request = requests.get(prefix+endPoint)
	r = request.json()
	
	divisionId = 0
	i = 0
	j = 0
	for div in r['records']:
		if div['division']['name'] == division:
			teams = r['records'][j]['teamRecords']
			for team in teams:
				standings.append(str(i+1) + ". " + teams[i]['team']['name'] +": " + str(teams[i]['points']) +"pts | " + "GP: " + str(teams[i]['gamesPlayed']) + " | " + "({}-{}-{})".format(teams[i]['leagueRecord']['wins'],teams[i]['leagueRecord']['losses'],teams[i]['leagueRecord']['ot']) + " | " + teams[i]['streak']['streakCode'])
				i = i+1
		j = j+1
		
	#print(standings)
	return standings

#getStandings('Central')
