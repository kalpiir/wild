def getSchedule(day):
	# Returns an array with specific day's games
	import requests
	from datetime import datetime, timedelta
	from pytz import timezone
	import pytz
	
	gameArray = []
	
	#Only works for today or tomorrow as of now
	if day == 'today':
		date = datetime.now()
	elif day == 'tomorrow':
		date = datetime.now() + timedelta(days=1)
	else:
		return 'Invalid date'
	
	date = datetime.strftime(date, "%Y-%m-%d")
	
	#Returns the games for a specific date
	prefix = 'https://statsapi.web.nhl.com/api/v1/schedule'
	endPoint = '?date='+date
	
	request = requests.get(prefix+endPoint)
	r = request.json()
	
	#Number of games today
	nrOfGames = r['totalGames']
	
	if nrOfGames == 0:
		gameArray.append("No games scheduled for " + day +":(")
	
	else:
		gameArray.append("Schedule for " + day + ":")
		#Get list of games
		games = r['dates'][0]['games']
		#Loop through
		for g in games:
			#Team names
			homeTeam = g['teams']['home']['team']['name']
			awayTeam = g['teams']['away']['team']['name']
			
			#Get date and time, comes in YYYY-MM-DDTHH:MM:SSZ format
			fullDate = g['gameDate']
			#Define timezones
			helsinki = timezone('Europe/Helsinki')
			utc = pytz.utc
			#Define format for date/time
			fmt = "%H:%M"
			#Extract date
			date = fullDate.split("T")[0].split("-")
			#Extract time and get rid of trailing Z
			time = fullDate.split("T")[1][:8]
			gameTime = datetime(int(date[0]), int(date[1]), int(date[2]),int(time.split(":")[0]), int(time.split(":")[1]), int(time.split(":")[2]), tzinfo=utc)
			#Convert to local time zone
			gameTimeLocal = gameTime.astimezone(helsinki)

			gameArray.append(gameTimeLocal.strftime(fmt)+": "+homeTeam+" - "+awayTeam)
			
	#for game in gameArray:
		#print(game)
	return gameArray;

#test = getSchedule('tomo')
#print(test)