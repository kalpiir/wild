# -*- coding: utf-8 -*-
import socket, string, feedparser, os, time, codecs
from threading import Timer

import getNext, getPrevious, getSchedule, getNextWeek, getStandings, getTopSix, getPace, nhlUtil

import sys  
reload(sys)  
sys.setdefaultencoding('utf8')

configured = False

configured = True
net = '127.0.0.1'
port = 27500
nick = 'isokissa2'
channel = '#0700'
ident = nick

readbuffer = ''
#connect the bot.
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect((net,port))

time.sleep(2)
s.send('USER '+ident+' '+net+' bla : '+ident+'\r\n')

time.sleep(2)
s.send('NICK '+nick+'\r\n')

#read channel data
ajaping = True
while(ajaping):
    readbuffer=readbuffer+s.recv(4096)
    temp=string.split(readbuffer, "\n")
    readbuffer=temp.pop()
    for line in temp:
        line=string.rstrip(line)
        line=string.split(line)
        print(line)
        if(line[0]=='PING'):
            s.send('PONG '+line[1]+'\r\n')
            ajaping = False

time.sleep(2)
s.send('JOIN '+channel+'\r\n')

#send a message to a channel
def msg(channel, msg):
	s.send('PRIVMSG '+str(channel)+' :'+str(msg)+'\r\n')

#read channel data
while(True):
    readbuffer=readbuffer+s.recv(4096)
    print readbuffer
    temp=string.split(readbuffer, "\n")
    readbuffer=temp.pop()
    for line in temp:
        line=string.rstrip(line)
        line=string.split(line)

#--Channel commands--
	if(line[0]=='PING'):
		s.send('PONG '+line[1]+'\r\n')
	if len(line) > 3:
		if(line[3].lstrip(':')=='help!'):
			for cmnd in nhlUtil.getCommands():
				msg(channel,cmnd)
		if(line[3].lstrip(':')=='wnext'): 
			var = getNext.getNext()
			msg(channel, var)
		if(line[3].lstrip(':')=='wprev'): msg(channel,getPrevious.getPrevious())
		if(line[3].lstrip(':')=='w7'): 
			wildSchedule = getNextWeek.getNextWeek()
			for game in wildSchedule:
				msg(channel,game)
		if(line[3].lstrip(':')=='today' or line[3].lstrip(':')=='tomorrow'):
			schedule = getSchedule.getSchedule(line[3].lstrip(':'))
			for game in schedule:
				msg(channel,game)
		if(line[3].lstrip(':')=='pace'):
			msg(channel,getPace.getPace('MIN'))
		if(line[3].lstrip(':')=='top6'):
			for team in getTopSix.getTopSix():
				msg(channel, team)
		if(line[3].lstrip(':')=='div'):
			if len(line) == 4:
				msg(channel,'Define division: Metropolitan, Atlantic, Central, Pacific')
			else: 
				standings = getStandings.getStandings(line[4])
				for team in standings:
					msg(channel,team)
			

