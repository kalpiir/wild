def getNextWeek():
	# Returns an array with Wild's (ID=30) games for the next week
	import requests
	import datetime
	from pytz import timezone
	import pytz
	
	#Define date range
	today = datetime.datetime.today()
	currentDate = "{}-{}-{}".format(today.year,today.month,today.day)
	delta = datetime.timedelta(days=7)
	oneWeekFromNow = today+delta
	endDate = "{}-{}-{}".format(oneWeekFromNow.year,oneWeekFromNow.month,oneWeekFromNow.day)
	
	#For testing purposes. Comment out when season starts.
	#currentDate = "2019-01-01"
	#endDate = "2019-01-15"

	# Returns Wild games for the next week
	prefix = "https://statsapi.web.nhl.com/api/v1/schedule?teamId=30&startDate="+currentDate+"&endDate="+endDate
	request = requests.get(prefix)
	r = request.json()
	
	gameArray = ["MNW schedule for the next 7 days:"]
	
	gameDates = r['dates']
	if r['totalGames']== 0:
		gameArray = ["No games scheduled for the next 7 days :("]
	else:
		for g in gameDates:
			homeTeam = g['games'][0]['teams']['home']['team']['name']
			awayTeam = g['games'][0]['teams']['away']['team']['name']
			
			#Get date and time, comes in YYYY-MM-DDTHH:MM:SSZ format
			fullDate = g['games'][0]['gameDate']
			#Define timezones
			helsinki = timezone('Europe/Helsinki')
			utc = pytz.utc
			#Define format for date/time
			fmt = "%d.%m.%Y %H:%M"
			#Extract date
			date = fullDate.split("T")[0].split("-")
			#Extract time and get rid of trailing Z
			time = fullDate.split("T")[1][:8]
			gameTime = datetime.datetime(int(date[0]), int(date[1]), int(date[2]),int(time.split(":")[0]), int(time.split(":")[1]), int(time.split(":")[2]), tzinfo=utc)
			#Convert to local time zone
			gameTimeLocal = gameTime.astimezone(helsinki)
		
			gameArray.append(gameTimeLocal.strftime(fmt)+": "+homeTeam+" - "+awayTeam)
		
	#for game in gameArray:
		#print(game)
	
	return gameArray;
		
#getNextWeek()