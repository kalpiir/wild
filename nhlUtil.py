# -*- coding: utf-8 -*-
"""
Created on Wed Dec  4 19:04:15 2019

@author: Kalle
"""

def getTeamId(teamAbb):
	import requests
	teamId = 0
	
	prefix = 'https://statsapi.web.nhl.com/api/v1/teams'
	request = requests.get(prefix)
	r = request.json()
	
	for team in r['teams']:
		if team['abbreviation'] == teamAbb:
			teamId = team['id']

	return teamId

	
def getCommands():
	commands = ['help! - Show commands','wnext - Next Wild game','wprev - Previous Wild game','w7 - Wild schedule for the next week','today - Schedule for today','div <Division> - Divisional standigs','top6 - League top6 + Wild', 'pace - Current pace']
	return commands
	


		
	
	