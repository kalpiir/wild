def getPrevious():
	# Returns the result for the last Wild game
	# Wild ID = 30
	import requests
	from datetime import datetime
	from pytz import timezone
	import pytz
	
	prefix = 'https://statsapi.web.nhl.com/api/v1'
	endPoint = '/teams/30/?expand=team.schedule.previous'
	request = requests.get(prefix+endPoint)
	r = request.json()
	
	#Get date and time, comes in YYYY-MM-DDTHH:MM:SSZ format
	fullDate = r['teams'][0]['previousGameSchedule']['dates'][0]['games'][0]['gameDate']
	#Define timezones
	helsinki = timezone('Europe/Helsinki')
	utc = pytz.utc
	#Define format for date/time
	fmt = "%d.%m.%Y"
	#Extract date
	date = fullDate.split("T")[0].split("-")
	#Extract time and get rid of trailing Z
	time = fullDate.split("T")[1][:8]

	gameTime = datetime(int(date[0]), int(date[1]), int(date[2]),int(time.split(":")[0]), int(time.split(":")[1]), int(time.split(":")[2]), tzinfo=utc)
	#Convert to local time zone
	gameTimeLocal = gameTime.astimezone(helsinki)
	
	#Get teams
	homeTeam = r['teams'][0]['previousGameSchedule']['dates'][0]['games'][0]['teams']['home']['team']['name']
	awayTeam = r['teams'][0]['previousGameSchedule']['dates'][0]['games'][0]['teams']['away']['team']['name']
	
	#Get records (W-L-OT). If OT not found then W-L
	homeTeamRecordFull = r['teams'][0]['previousGameSchedule']['dates'][0]['games'][0]['teams']['home']['leagueRecord']
	awayTeamRecordFull = r['teams'][0]['previousGameSchedule']['dates'][0]['games'][0]['teams']['away']['leagueRecord']
	try:
		homeTeamRecord = "({}-{}-{})".format(homeTeamRecordFull['wins'],homeTeamRecordFull['losses'],homeTeamRecordFull['ot'])
		awayTeamRecord = "({}-{}-{})".format(awayTeamRecordFull['wins'],awayTeamRecordFull['losses'],awayTeamRecordFull['ot'])
	except KeyError:
		homeTeamRecord = "({}-{})".format(homeTeamRecordFull['wins'],homeTeamRecordFull['losses'])
		awayTeamRecord = "({}-{})".format(awayTeamRecordFull['wins'],awayTeamRecordFull['losses'])
    
	#Get end score
	homeTeamGoals = "{}".format(r['teams'][0]['previousGameSchedule']['dates'][0]['games'][0]['teams']['home']['score'])
	awayTeamGoals = "{}".format(r['teams'][0]['previousGameSchedule']['dates'][0]['games'][0]['teams']['away']['score'])
	
	prevGame = gameTimeLocal.strftime(fmt)+": "+homeTeam+homeTeamRecord+" - "+awayTeam+awayTeamRecord+": "+homeTeamGoals+"-"+awayTeamGoals
	#print(prevGame)
	return prevGame;
	
#getPrevious()