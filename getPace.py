# -*- coding: utf-8 -*-
"""
Created on Wed Dec  4 19:15:08 2019

@author: Kalle
"""


def getPace(teamAbb):
		
	import nhlUtil
	import requests
	
	teamId = nhlUtil.getTeamId(teamAbb)
	if teamId == 0:
		return 'No team found'
	
	prefix = 'https://statsapi.web.nhl.com/api/v1/teams/'
	endPoint = str(teamId)+'?expand=team.stats'
	request = requests.get(prefix+endPoint)
	r = request.json()
	
	teamStats = r['teams'][0]['teamStats'][0]['splits'][0]['stat']
	points = teamStats['pts']
	gp = teamStats['gamesPlayed']
	ptsPct = teamStats['ptPctg']
	pace = round(float(ptsPct)/100 * (82-int(gp)) * 2 + int(points),1)
	
	currentPace = "GP: {} | PTS: {} | Current pace: {}".format(gp,points,pace)
	
	#print(currentPace)
	return currentPace

#getPace("MIN")
